package tech.aliskm.bluetoothtest.old;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;

import tech.aliskm.bluetoothtest.R;
import tech.aliskm.bluetoothtest.old.HelperClass;

import static tech.aliskm.bluetoothtest.old.HelperClass.keepLogE;

public class MainActivityTwo extends AppCompatActivity {


    private static final int REQUEST_ENABLE_BT = 901;
    private TextView tvTextOutput;
    private SwitchCompat swBTOnOff;
    private BluetoothAdapter mBluetoothAdapter;
    private AcceptThread acceptThread;
//    private String uniqueId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_two);

        init();
        checkMethod();
        onClick();

    }

    private void init() {
        tvTextOutput = findViewById(R.id._tvOutput);
        swBTOnOff = findViewById(R.id._swBtOnOff);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        acceptThread = new AcceptThread();
    }

    private void checkMethod() {
        //--checking if has bluetooth--
        if (mBluetoothAdapter == null) {
            keepLogE("check", "Device doesn't support Bluetooth");
            tvTextOutput.setText("no bluetooth device found");
            swBTOnOff.setEnabled(false);
        } else {
            keepLogE("check", "Device has bluetooth");
            swBTOnOff.setEnabled(true);
        }

        //--checking if bluetooth is on--
        if (!mBluetoothAdapter.isEnabled()) {
            keepLogE("check", "bluetooth is off");
            swBTOnOff.setChecked(false);
        } else {
            keepLogE("check", "bluetooth is on");
            swBTOnOff.setChecked(true);
        }
    }

    private void onClick() {

        //--bluetooth switch on/off--
        swBTOnOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                }
                if (!isChecked) {
                    mBluetoothAdapter.cancelDiscovery();
                    mBluetoothAdapter.disable();
                    if (mBluetoothAdapter == null)
                        keepLogE("click", "mBluetoothAdapter result: null");
                    else {
                        keepLogE("click", "mBluetoothAdapter result: not null");
                    }
                }
            }
        });

    }


    public void _showPairedDevice(View view) {

        //--uniqueID--
//        uniqueId = UUID.randomUUID().toString();
//        keepLogE("UUID",""+uniqueId);

        //--discover bluetooth devices--
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        StringBuilder sb = new StringBuilder();
        if (pairedDevices.size() > 0) {

            for (BluetoothDevice device : pairedDevices) {
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress(); // MAC address
                sb.append(deviceName).append(" ").append(deviceHardwareAddress);
            }
            tvTextOutput.setText(sb.toString());
        }
    }

    private class AcceptThread extends Thread {
        private final BluetoothServerSocket mmServerSocket;

        public AcceptThread() {
            // Use a temporary object that is later assigned to mmServerSocket
            // because mmServerSocket is final.
            BluetoothServerSocket tmp = null;
            try {
                tmp = mBluetoothAdapter.listenUsingRfcommWithServiceRecord(HelperClass.SERVER_NAME, UUID.fromString(HelperClass.UUID));
            } catch (IOException e) {
                keepLogE("accept_thread","Socket's listen() method failed");
            }
            mmServerSocket = tmp;
        }

        public void run() {
            BluetoothSocket socket = null;
            // Keep listening until exception occurs or a socket is returned.
            while (true) {
                try {
                    socket = mmServerSocket.accept();
                } catch (IOException e) {
                    keepLogE("accThreadRun", "Socket's accept() method failed");
                    break;
                }

                if (socket != null) {
                    try {
                        // A connection was accepted. Perform work associated with
                        // the connection in a separate thread.
//                        manageMyConnectedSocket(socket);
                        mmServerSocket.close();
                        break;
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }

        // Closes the connect socket and causes the thread to finish.
        public void cancel() {
            try {
                mmServerSocket.close();
            } catch (IOException e) {
                keepLogE("cancleThread", "Could not close the connect socket");
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            if (mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()) {
                mBluetoothAdapter.cancelDiscovery();
                mBluetoothAdapter.disable();
                keepLogE("closing", "onStop called and disabling bluetooth sensor");
                acceptThread.cancel();
                keepLogE("cancleAcceptThread","canceling accept thread");
            }
        }catch (Exception e){e.printStackTrace();}
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 901) {
            keepLogE("permission", "permission accepted: requestCode:" + requestCode + " resultCode:" + resultCode);
        } else {
            keepLogE("permission", "permission denied: requestCode:" + requestCode + " resultCode:" + resultCode);
        }
        if (mBluetoothAdapter.isEnabled()) {
            Toast.makeText(this, "bluetooth is on", Toast.LENGTH_SHORT).show();
        }
    }

}
